/*
    Firmware for custom Synthi 256 sequencer board.
    Copyright (C) 2016  Jari Suominen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <MIDI.h>
#include <EEPROM.h>

MIDI_CREATE_DEFAULT_INSTANCE();

/*
  SYNTHI 100 Sequencer AVR implementation
*/

/* 
 *  FUSES: HF: 0xDE LF: 0xFF EF: 0xFD
 *  avrdude -c avrispmkII -p m644p -P /dev/ttyACM0 -U lfuse:w:0xff:m -U hfuse:w:0xde:m -U efuse:w:0xfd:m -F
 *  
 */


/* INPUTS AND OUTPUTS ref. Sequencer 256 CARD FRAME (BACK WIRING) */
/* INPUTS: (*18) */

/* PORT PB from 0 to 7 */
#define SWITCH_STOP_AT_EVENT_END 0
#define START_REMOTE 1    /* Starting sequencer. The direction of the sequencer will not be touched. */
#define STOP_BUTTON_REMOTE 2    /* Stops counter. Also remote CV! */
#define CLOCK_IN 5
#define RESET_BUTTON_REMOTE 6   /* Returns display instantly to 0000 (but does not stop the clock. ALSO remote CV!*/
#define STT_REV_BUTTON 7 /* The counter counts down one for every pulse in CLOCK input. */


/* PORT PA from 0 to 7. */
#define ACE_IN A0
#define BDF_IN A1
#define SEM_SWITCH 26 /* Special Edit Mode = rewrite BDF Switch */
#define KEY_INPUT 27
#define SWITCH_STOP_AT_EACH_EVENT 28
#define MS1 29
#define MS2 30
#define MS3 31

#define SEM_SWITCH_                 B00000100
#define KEY_INPUT_                  B00001000
#define SWITCH_STOP_AT_EACH_EVENT_  B00010000
#define MS2_                        B00100000
#define MS1_                        B01000000
#define MS3_                        B10000000
#define PA_INPUT_MASK               B11111100

/* PORT PB These are for faster digitalread purposes */
#define SWITCH_STOP_AT_EVENT_END_ B00000001
#define START_REMOTE_             B00000010
#define STOP_BUTTON_REMOTE_       B00000100
#define CLOCK_IN_                 B00100000
#define RESET_BUTTON_REMOTE_      B01000000
#define STT_REV_BUTTON_           B10000000
#define PB_INPUT_MASK             B11100111

/* PORT PD from 0 to 2 */
#define MIDI_IN 8       /* Only used through MIDI library */
#define MIDI_OUT 9      /* Only used through MIDI library */
#define STT_FWD_BUTTON 10 /* The counter counts up one for every pulse in CLOCK input. */

#define STT_FWD_BUTTON_           B00000100
#define PD_INPUT_MASK             B00000100


/* PORT PC from 6 to 7 */
#define CMB_BUTTON 22     /* Clear Memory Button. Also resets the clock, although in synthi hardwired to reset button as well */
#define ECB_BUTTON 23     /* Erase Current event Button */

#define CMB_BUTTON_               B01000000
#define ECB_BUTTON_               B10000000
#define PC_INPUT_MASK             B11000000

/* OUTPUTS (*14) */

// NIXIE DISPLAY OUTPUT
#define DECADE_COUNTER_DOWN_DCP 19
#define DECADE_COUNTER_UP_UCP 20
#define DECADE_COUNTER_RESET_ZOB 21

// PWM OUTPUTS!
#define A_OUT 15
#define B_OUT 14
#define C_OUT 13
#define D_OUT 12
#define E_OUT 4
#define F_OUT 3

// TRIGGER OUTPUTS
#define KEY_1_OUT 16
#define KEY_2_OUT 17
#define KEY_3_OUT 18
#define KEY_4_OUT 11

/* KEY outs 1-3 in PC, 4 in in PD. */
#define KEY_1_OUT_ B00000001
#define KEY_2_OUT_ B00000010
#define KEY_3_OUT_ B00000100
#define KEY_4_OUT_ B00001000

// MIDI
byte midiClockCounter = 0;
byte midiInChannel = 1;
byte midiOutChannel = 1;

boolean previousButtonStates[32]; /* Soon OBSOLETE */
byte prevPA = PA_INPUT_MASK;
byte prevPB = PB_INPUT_MASK;
byte prevPC = PC_INPUT_MASK;
byte prevPD = PD_INPUT_MASK;
#define MIDI_TICKS_PER_8TH 6

/* ADC input buffers will create slight DC offset
    and attenuate signal a bit to protect the adc inputs
    so will have to do a bit of scaling when reading.
*/
#define ADC_MIN 89
#define ADC_MAX 931

/* For every CV trig input we have to do a bit of filtering. */
byte hysteresis[5] = { 0, 0, 0, 0, 0};
#define HYSTERESIS_RESET_BUTTON_REMOTE 0
#define HYSTERESIS_CLOCK_IN 1
#define HYSTERESIS_STOP_BUTTON_REMOTE 2
#define HYSTERESIS_START_REMOTE 3
#define HYSTERESIS_KEY_INPUT 4
#define HYSTERESIS_TH 30


/* Current implementation, 100Hz highest clock rate. Should push it up to
   400Hz in the future! ( > 2ms ) */

int step = 0; /* Setting of the timer */

#define MEMORYSIZE 256
#define PULSE_LENGTH 100

int todo[] = {A_OUT, B_OUT, C_OUT, D_OUT, E_OUT, F_OUT};

int eventStart[MEMORYSIZE];
int eventEnd[MEMORYSIZE];
byte eventLayer[MEMORYSIZE];
byte eventV1[MEMORYSIZE];
byte eventV2[MEMORYSIZE];

boolean forward = true;
boolean stopped = true;
boolean midiStopped = true;

byte midiNoteBeingRecorded = 255;
byte midiVelocityBeingRecorded = 255;

byte memoryCounter = 0;

void setup() {
  /* Fast-PWM on every pin, pwm frequency ~62500Hz */

  TCCR0A = _BV(COM0A1) | _BV(COM0B1) | _BV(WGM01) | _BV(WGM00);
  TCCR0B = (TCCR0B & 0b11110000) | 0x01;
  TCCR1A = B10100001;
  TCCR1B = B00001001;
  TCCR2A = _BV(COM2A1) | _BV(COM2B1) | _BV(WGM21) | _BV(WGM20);
  TCCR2B = (TCCR2B & 0b11111000) | 0x01;

  /*TCCR1A = B10100001;
    TCCR1B = B00000001;
    TCCR2A = _BV(COM2A1) | _BV(COM2B1) | _BV(WGM20);
    TCCR2B = (TCCR2B & 0b11111000) | 0x01;*/
  /*TCCR0A = _BV(COM0A1) | _BV(COM0B1) | _BV(WGM00);
    TCCR0B = (TCCR0B & 0b11110000) | 0x01;
    TCCR1A = B10100001;
    TCCR1B = B00000001;
    TCCR2A = _BV(COM2A1) | _BV(COM2B1) | _BV(WGM20);
    TCCR2B = (TCCR2B & 0b11111000) | 0x01;*/


  MIDI.begin(MIDI_CHANNEL_OMNI);
  MIDI.setHandleClock(midiClock);
  MIDI.setHandleStart(midiStart);
  MIDI.setHandleContinue(midiContinue); // NEVER!
  MIDI.setHandleStop(midiStop);
  MIDI.setHandleNoteOn(midiNoteOn);
  MIDI.setHandleNoteOff(recordEventStop);
  MIDI.setHandleControlChange(midiCC);
  //MIDI.turnThruOff();

  pinMode(KEY_INPUT, INPUT_PULLUP);
  pinMode(STT_FWD_BUTTON, INPUT_PULLUP);
  pinMode(STT_REV_BUTTON, INPUT_PULLUP);
  pinMode(STOP_BUTTON_REMOTE, INPUT_PULLUP);
  pinMode(RESET_BUTTON_REMOTE, INPUT_PULLUP);
  pinMode(ECB_BUTTON, INPUT_PULLUP);
  pinMode(CMB_BUTTON, INPUT_PULLUP);
  pinMode(START_REMOTE, INPUT_PULLUP);
  pinMode(SWITCH_STOP_AT_EACH_EVENT, INPUT);
  pinMode(SWITCH_STOP_AT_EVENT_END, INPUT);
  pinMode(SEM_SWITCH, INPUT);
  pinMode(CLOCK_IN, INPUT_PULLUP);
  pinMode(KEY_INPUT, INPUT_PULLUP);
  pinMode(MS1, INPUT);
  pinMode(MS2, INPUT);
  pinMode(MS3, INPUT);

  pinMode(DECADE_COUNTER_DOWN_DCP, OUTPUT);
  pinMode(DECADE_COUNTER_UP_UCP, OUTPUT);
  pinMode(DECADE_COUNTER_RESET_ZOB, OUTPUT);
  digitalWrite(DECADE_COUNTER_DOWN_DCP, HIGH);
  digitalWrite(DECADE_COUNTER_UP_UCP, HIGH);
  digitalWrite(DECADE_COUNTER_RESET_ZOB, HIGH);

  for (int i = 0; i < 6; i++) {
    /* switching pwms on, just writing a value between 1 and 254. */
    analogWrite(todo[i], 127);
  }

  pinMode(KEY_1_OUT, OUTPUT);
  pinMode(KEY_2_OUT, OUTPUT);
  pinMode(KEY_3_OUT, OUTPUT);
  pinMode(KEY_4_OUT, OUTPUT);
  digitalWrite(SEM_SWITCH, HIGH);
  digitalWrite(SWITCH_STOP_AT_EACH_EVENT, HIGH);
  digitalWrite(SWITCH_STOP_AT_EVENT_END, HIGH);

  DDRA = DDRA & ~PA_INPUT_MASK;   /* BOARD Definitions don't call up all the way! */
  PORTA = PORTA | PA_INPUT_MASK; /* Manually setting up the highest pins to the read mode */

  //DDRD = DDRA & ~PD_INPUT_MASK;
  //PORTD = PORTA | PD_INPUT_MASK;

  prevPA = PINA;
  prevPB = PINB;
  prevPC = PINC;
  prevPD = PIND;

  clearMemory();
  //sequencerStart();

  midiInChannel = EEPROM.read(0);
  midiOutChannel = EEPROM.read(1);

  if (midiInChannel == 255) {
    midiInChannel = 1;
    EEPROM.write(0, 1);
  }
  if (midiOutChannel == 255) {
    midiOutChannel = 1;
    EEPROM.write(1, 1);
  }
}


int recordHead = -1;
byte aaaa = 0;
boolean dirtyWrite = false;
boolean midiOut = false;
boolean updateMidiOut = false;
boolean updateMidiIn = false;

byte prevACE = 255;
byte prevBDF = 255;
byte prevKEY = 255;
boolean preKey = false;


void loop() {

  MIDI.read();

  //MIDI.sendPitchBend((int)random(15),1); // DEBUG

  /* Lets read through all the inputs. We want to do this ultra-fast
    so we will read all ports at once and then parse the information
    we need with some bit magic. */
  /* Note these input masks probably aren't used in anything usefull really? */
  byte pa = PINA & PA_INPUT_MASK;
  byte pb = PINB & PB_INPUT_MASK;
  byte pc = PINC & PC_INPUT_MASK;
  byte pd = PIND & PD_INPUT_MASK;

  /* Here we will see which pins have changed since last iteration. */
  //byte xa = pa ^ prevPA; /* unused */
  byte xb = pb ^ prevPB;
  byte xc = pc ^ prevPC;
  byte xd = pd ^ prevPD;

  //prevPA = pa;
  prevPB = pb;
  prevPC = pc;
  prevPD = pd;

  /* OK! When scanning through inputs, we don't optimize shit!
     We want every iteration being more or less identical. */

  /* Scanning the buttons */

  if (xd & STT_FWD_BUTTON_) {
    if (!(pb & STOP_BUTTON_REMOTE_) && (hysteresis[HYSTERESIS_STOP_BUTTON_REMOTE] == 0) && (!(pd & STT_FWD_BUTTON_))) {
      updateMidiIn = true;
      nixiesShowInt(midiInChannel);
      while (!(PINB & STOP_BUTTON_REMOTE_) || !(PIND & STT_FWD_BUTTON_)) {
        MIDI.read();
      }
      sequencerReset();
      updateMidiIn = false;
    } else {
      forward = true;
      sequencerStart();
    }
  }

  if (xb & STT_REV_BUTTON_) {
    if ((!(pb & STOP_BUTTON_REMOTE_)) && (!(PINB & STT_REV_BUTTON_)) && (hysteresis[HYSTERESIS_STOP_BUTTON_REMOTE] == 0)) {
      midiOut = !midiOut;
      prevACE = 255;
      prevBDF = 255;
      updateMidiOut = true;
      nixiesShowInt(midiOutChannel);
      while (!(PINB & STOP_BUTTON_REMOTE_) || !(PINB & STT_REV_BUTTON_)) {
        MIDI.read();
      }
      updateMidiOut = false;
      sequencerReset();
    } else {
      forward = false;
      sequencerStart();
    }
  }

  if (xc & CMB_BUTTON_) {
    clearMemory();
  }

  if (xb & RESET_BUTTON_REMOTE_) hysteresis[HYSTERESIS_RESET_BUTTON_REMOTE] = HYSTERESIS_TH;
  else if ((hysteresis[HYSTERESIS_RESET_BUTTON_REMOTE] == 1) && (!(pb & RESET_BUTTON_REMOTE_))) sequencerReset();

  if (xb & CLOCK_IN_) hysteresis[HYSTERESIS_CLOCK_IN] = HYSTERESIS_TH;
  else if ((hysteresis[HYSTERESIS_CLOCK_IN] == 1) && (!(pb & CLOCK_IN_))) sequencerAdvance();

  if (xb & STOP_BUTTON_REMOTE_) hysteresis[HYSTERESIS_STOP_BUTTON_REMOTE] = HYSTERESIS_TH;
  else if ((hysteresis[HYSTERESIS_STOP_BUTTON_REMOTE] == 1) && (!(pb & STOP_BUTTON_REMOTE_))) sequencerStop();

  if (xb & START_REMOTE_) hysteresis[HYSTERESIS_START_REMOTE] = HYSTERESIS_TH;
  else if ((hysteresis[HYSTERESIS_START_REMOTE] == 1) && (!(pb & START_REMOTE_))) sequencerStart();

  if (xb & KEY_INPUT_) hysteresis[HYSTERESIS_KEY_INPUT] = HYSTERESIS_TH;

  /* Optimizing so getting rid of the loops! */
  if (hysteresis[0]) hysteresis[0]--;
  if (hysteresis[1]) hysteresis[1]--;
  if (hysteresis[2]) hysteresis[2]--;
  if (hysteresis[3]) hysteresis[3]--;
  if (hysteresis[4]) hysteresis[4]--;


  /* ED SWITCH */
  byte layerSelect = 0;
  if (!(pa & MS1_)) { // This could be simpler equation? Like above with the SEM_SWITCH?
    layerSelect = 0;
  } else if (!(pa & MS2_)) {
    layerSelect = 1;
  } else if (!(pa & MS3_)) {
    layerSelect = 2;
  } else {
    layerSelect = 3;
  }


  /* READ-IN */
  if (recordHead == -1) {
    if ((!(pa & KEY_INPUT_)) || midiNoteBeingRecorded != 255) {
      if (dirtyWrite) {
        if ((eventStart[memoryCounter] != -1) && (eventEnd[memoryCounter] == -1)) {
          recordHead = memoryCounter;
        }
      } else if (eventStart[memoryCounter] == -1) {
        recordHead = memoryCounter;
      }
      if (recordHead != -1) {
        eventStart[recordHead] = step;
        dirtyWrite = false;
        if (midiOut && !(pa & KEY_INPUT_) && (prevKEY != true)) {
          prevKEY = true;
          MIDI.sendControlChange(22, 127, midiOutChannel);
        }
      }
    }
  }

  if ((recordHead != -1) && (memoryCounter == recordHead)) {

    /* We are recording! */
    eventLayer[recordHead] = layerSelect; /* This will actually have surprising outcomes, but it is how the real thing works! */
    if (eventLayer[recordHead] != 3) {
      if (midiNoteBeingRecorded != 255) { /* We are getting values through MIDI. */
        eventV1[recordHead] = map(constrain(midiNoteBeingRecorded, 35, 98), 35, 98, 0, 63); /* Mapping MIDI notes as real Synthi keyboards when optimally set. */
        if (midiVelocityBeingRecorded != 255) {
          byte a = adcRead(BDF_IN, 63);
          if (analogRead(BDF_IN) < 50) {
            eventV2[recordHead] = midiVelocityBeingRecorded >> 1;
          } else {
            eventV2[recordHead] = adcRead(BDF_IN, 63);
          }
        }
      } else {
        eventV1[recordHead] = adcRead(ACE_IN, 63); /* 6 bits it is! */
        eventV2[recordHead] = adcRead(BDF_IN, 63);
      }
    }

    if (((midiNoteBeingRecorded == 255) && (pa & KEY_INPUT_)) || ((midiNoteBeingRecorded != 255) && (midiVelocityBeingRecorded == 255))) {
      /* KEY RELEASED */

      if (eventStart[recordHead] == step) {
        /* Sequencer is still at the same step, this open event might be overwritten in the future! */
        trigWrite(eventLayer[recordHead], false);
        dirtyWrite = true;
      } else {
        eventEnd[recordHead] = step;
        dirtyWrite = false;
      }
      if (midiOut && (pa & KEY_INPUT_) && (midiNoteBeingRecorded == 255)) {
        prevKEY = false;
        MIDI.sendControlChange(22, 0, midiOutChannel);
      }
      recordHead = -1;
      midiNoteBeingRecorded = 255;
    }
  }

  /* READ-OUT */
  if (eventStart[memoryCounter] == step) {
    if ((pa & SWITCH_STOP_AT_EACH_EVENT_) && (eventLayer[memoryCounter] == layerSelect) && (eventEnd[memoryCounter] != -1)) stopped = true;
    if (!(pc & ECB_BUTTON_) && (eventLayer[memoryCounter] == layerSelect) && (eventEnd[memoryCounter] != -1)) {
      /* Erase event function. */
      eventStart[memoryCounter] = -1;
      eventEnd[memoryCounter] = -1;
      trigWrite(layerSelect, false);
    } else if (eventEnd[memoryCounter] != -1 || !dirtyWrite) {
      trigWrite(eventLayer[memoryCounter], true);
      if (eventLayer[memoryCounter] < 3) {
        if ((eventLayer[memoryCounter] == layerSelect) && (pa & SEM_SWITCH_)) {
          eventV2[memoryCounter] = adcRead(BDF_IN, 63);
        }
        /* It seems that there is too much rounding problems with mapping, so will
         *  have to do exact bitshift instead and scaling in the analog eletronics.
         */
        //dacWrite(eventLayer[memoryCounter] * 2, map(eventV1[memoryCounter], 0, 63, 0, 255));
        //dacWrite(eventLayer[memoryCounter] * 2 + 1, map(eventV2[memoryCounter], 0, 63, 0, 255));
        dacWrite(eventLayer[memoryCounter] * 2, eventV1[memoryCounter]<<2);
        dacWrite(eventLayer[memoryCounter] * 2 + 1, eventV2[memoryCounter]<<2);
      }
    }
  }

  if (dirtyWrite && (eventStart[memoryCounter] != step) && (eventStart[memoryCounter] != -1) && (eventEnd[memoryCounter] == -1)) {
    /* Closing event if we did not write it yet! */
    dirtyWrite = false;
    eventEnd[memoryCounter] = step;
    MIDI.sendNoteOn(1, 2, 3);
  }

  if (eventEnd[memoryCounter] == step) {
    if ((pb & SWITCH_STOP_AT_EVENT_END_) && (eventLayer[memoryCounter] == layerSelect)) stopped = true;
    trigWrite(eventLayer[memoryCounter], false);
  }

  if (midiOut && !memoryCounter) {
    /* If midiOut is on, will send out CC data ~100 times per second. */
    byte b = adcRead(ACE_IN, 127);
    if (b != prevACE) {
      prevACE = b;
      MIDI.sendControlChange(20, b, midiOutChannel);
    }
    b = adcRead(BDF_IN, 127);
    if (b != prevBDF) {
      prevBDF = b;
      MIDI.sendControlChange(21, b, midiOutChannel);
    }
  }

  memoryCounter++;

  //if (!memoryCounter) PINC = KEY_1_OUT_; /* DEBUG ROW */

  return;
}


void clearMemory() {
  for (int i = 0; i < MEMORYSIZE; i++) {
    eventStart[i] = -1;
    eventEnd[i] = -1;
    eventV1[i] = 0;
    eventV2[i] = 0;
    eventLayer[i] = 0;
  }

  for (int i = 0; i < 4; i++) {
    trigWrite(i, false);
  }

  sequencerReset();
  recordHead = -1;
  dirtyWrite = false;
  sequencerStop();
  midiStop();
}


void sequencerAdvance() {

  if (!stopped || !midiStopped) {
    if (forward) {
      step++;
      step %= 10000;
      // send up to nixies
      digitalWrite(DECADE_COUNTER_UP_UCP, LOW);
      delayMicroseconds(PULSE_LENGTH);
      digitalWrite(DECADE_COUNTER_UP_UCP, HIGH);
    }
    else {
      step--;
      if (step == -1) step = 9999;
      // send down to nixies
      digitalWrite(DECADE_COUNTER_DOWN_DCP, LOW);
      delayMicroseconds(PULSE_LENGTH);
      digitalWrite(DECADE_COUNTER_DOWN_DCP, HIGH);
    }
    //MIDI.sendRealTime(0xF8);
    //MIDI.sendPitchBend(step,1); //DEBUG
  }
}


void sequencerStop() {
  if (!stopped) {
    stopped = true;
    //MIDI.sendRealTime(0xFC);
  }
}


void sequencerStart() {
  if (stopped && midiStopped) {
    stopped = false;
    //MIDI.sendRealTime(0xFA);
  }
}


void sequencerReset() {
  step = 0;
  midiClockCounter = 0;
  digitalWrite(DECADE_COUNTER_RESET_ZOB, LOW);
  delayMicroseconds(PULSE_LENGTH);
  digitalWrite(DECADE_COUNTER_RESET_ZOB, HIGH);
}


void sequencerResetStart() {
  sequencerReset();
  sequencerStart();
}


void recordEventStart(byte channel, byte pitch, byte velocity ) {
  if (velocity != 0) {
    midiNoteBeingRecorded = pitch;
    midiVelocityBeingRecorded = velocity;
  } else {
    recordEventStop(channel, pitch, velocity);
  }
}


void recordEventStop(byte channel, byte pitch, byte velocity) {
  if (midiInChannel == channel)
    midiVelocityBeingRecorded = 255;
}


void midiClock() {
  if (midiStopped) return;
  if (midiClockCounter == 0) sequencerAdvance();
  midiClockCounter++;
  midiClockCounter %= MIDI_TICKS_PER_8TH;
}

void midiStart() {
  if (stopped) {
    sequencerReset();
    midiStopped = false;
  }
}

void midiStop() {
  midiStopped = true;
  midiClockCounter = 0;
}

void midiContinue() {
  if (stopped) {
    midiStopped = false;
  }
}


void midiNoteOn(byte channel, byte pitch, byte velocity) {
  if (updateMidiOut) {
    if (midiOutChannel != channel) {
      midiOutChannel = channel;
      EEPROM.write(1, channel);
      nixiesShowInt(channel);
    }
    return;
  }
  if (updateMidiIn) {
    if (midiInChannel != channel) {
      midiInChannel = channel;
      EEPROM.write(0, channel);
      nixiesShowInt(channel);
    }
    return;
  }
  if (channel == midiInChannel) {
    recordEventStart(channel, pitch, velocity);
  }
}


/* CC Interface. Controls from 20-29. 7-bit/1-bit data straight to outputs. */
void midiCC(byte channel, byte control, byte value) {

  if (channel == midiInChannel) {
    switch (control) {
      case 20: /* A */
        dacWrite(0, map(value, 0, 127, 0, 255));
        break;
      case 21: /* B */
        dacWrite(1, map(value, 0, 127, 0, 255));
        break;
      case 22: /* 1 */
        if (value > 63) {
          trigWrite(0, true);
        } else {
          trigWrite(0, false);
        }
        break;
      case 23: /* C */
        dacWrite(2, map(value, 0, 127, 0, 255));
        break;
      case 24: /* D */
        dacWrite(3, map(value, 0, 127, 0, 255));
        break;
      case 25: /* 2 */
        if (value > 63) {
          trigWrite(1, true);
        } else {
          trigWrite(1, false);
        }
        break;
      case 26: /* E */
        dacWrite(4, map(value, 0, 127, 0, 255));
        break;
      case 27: /* F */
        dacWrite(5, map(value, 0, 127, 0, 255));
        break;
      case 28: /* 3 */
        if (value > 63) {
          trigWrite(2, true);
        } else {
          trigWrite(2, false);
        }
        break;
      case 29: /* 4 */
        if (value > 63) {
          trigWrite(3, true);
        } else {
          trigWrite(3, false);
        }
        break;
    }
  }
}


void dacWrite(int dac, byte val) {
  switch (dac) {
    case 0:
      OCR2B = val;
      break;
    case 1:
      OCR2A = val;
      break;
    case 2:
      OCR1B = val;
      break;
    case 3:
      OCR1A = val;
      break;
    case 4:
      OCR0A = val;
      break;
    case 5:
      OCR0B = val;
      break;
  }
}


void trigWrite(int key, boolean on) {
  switch (key) {
    case 0: if (on) {
        PORTC = PORTC | KEY_1_OUT_;
      } else {
        PORTC = PORTC & ~KEY_1_OUT_;
      }
      break;
    case 1: if (on) {
        PORTC = PORTC | KEY_2_OUT_;
      } else {
        PORTC = PORTC & ~KEY_2_OUT_;
      }
      break;
    case 2: if (on) {
        PORTC = PORTC | KEY_3_OUT_;
      } else {
        PORTC = PORTC & ~KEY_3_OUT_;
      }
      break;
    case 3: if (on) {
        PORTD = PORTD | KEY_4_OUT_;
      } else {
        PORTD = PORTD & ~KEY_4_OUT_;
      }
      break;
  }
}


byte adcRead(int pin, byte scale) {
  return (int)map(constrain(analogRead(pin), ADC_MIN, ADC_MAX), ADC_MIN, ADC_MAX, 0, scale);
}

void nixiesShowInt(int value) {
  digitalWrite(DECADE_COUNTER_RESET_ZOB, LOW);
  delayMicroseconds(PULSE_LENGTH);
  digitalWrite(DECADE_COUNTER_RESET_ZOB, HIGH);
  delayMicroseconds(PULSE_LENGTH);
  int i = 0;
  while (i < value) {
    digitalWrite(DECADE_COUNTER_UP_UCP, LOW);
    delayMicroseconds(PULSE_LENGTH);
    digitalWrite(DECADE_COUNTER_UP_UCP, HIGH);
    delayMicroseconds(PULSE_LENGTH);
    i++;
  }
}

